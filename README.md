![Intro](https://www.elixir-europe.org/sites/default/files/images/software-data-carpentry.png)

# Data Carpentry and Software Carpentry Workshop within Wageningen UR
This workshop is geared towards Food Safety Scientists who work with data and models. With this workshop we aim to provide best practices for managing data and models to make them reusable and reproducible.


The workshop is a mix of lectures and hand-on lessons where you practice giving a short lesson using approaches learned.

This is training motivation based, and we hope to convince you that using this material you will become more effective and efficient on daily activities related with data and model.
For Day 1 no specific technical background is necessary. However on Day 2 we encourage participants have basic knowledge of R.
Presentation Material and cheatsheets can be found within this repository.


### Date : 
April 09-10 2018, 09:00-16:00 (TBD)

### Location :
TBD

### Instructors:
Gwen Dawes, Mateusz Kuzak, Anand Gavai, Others

### Helpers: 
TBD

### Requirements:
Participants should bring a laptop that is Internet connected and has a functioning browser. 
If you have it, a specialized device for recording audio and video (mobile phones and laptops are OK).

### Contact
Please contact [Anand Gavai](anand.gavai@wur.nl) for more information

# Preparation
If you wish to prepare yourself consider going through [Software Carpentry Lesson Page](https://software-carpentry.org/lessons/)

# Organization
The workshop is organized into two parts 

## Day 1 : Introduction to Linux and GIT
| 09:00 | Welcome and overview                   |
|-------|----------------------------------------|
| 09:15 | Introduction to Linux                  |
| 09:30 | File System in Linux                   |
| 10:00 | Hands-on exercises                     |
| 10:30 | Coffee break                           |
| 10:45 | Introduction to Bash Shell             |
| 11:00 | Assembling commands in Bash script     |
| 11:15 | Hands-on exercises                     |
| 12:00 | Lunch Break                            |
| 13:00 | Introduction to Version Control system |
| 13:15 | Introduction to GIT (Local)            |
| 13:30 | Hands-on exercises (GIT Local)         |
| 14:30 | Coffee break                           |
| 14:45 | Introduction to GIT (Remote)           |
| 15:00 | Hands-on exercises (GIT Remote)        |
| 15:45 | Wrap-up                                |

## Take Home Message
But the end of Day 1 participants will be able to use linux file system and will be familiar with most commonly used GIT commands that would serve their daily activities. 


## Day 2 : Reusable and Reproducible Software using R 

| 09:00 | Recap from previous day                                     |
|-------|-------------------------------------------------------------|
| 09:15 | Introduction to Notebooks                                   |
| 09:30 | Data Structures                                             |
| 10:00 | Hands-on exercises                                          |
| 10:30 | Coffee Break                                                |
| 10:45 | Dataframe Manipulation with dplyr                           |
| 11:00 | Dataframe Manipulation with tidyr                           |
| 11:15 | Hands-on exercises                                          |
| 12:00 | Lunch Break                                                 |
| 13:00 | Writting Data                                               |
| 13:15 | Producing Reproducible reports using Markdown and Notebooks |
| 13:30 | Hands-on exercises                                          |
| 14:30 | Coffee break                                                |
| 14:45 | Writting Good Models                                        |
| 15:00 | Hands-on exercises                                          |
| 15:45 | Wrap-up                                                     |

## Take Home Message
But the end of Day 2 participants will be able to make their models robust and best practices for making their models reusable and reproducible. 
